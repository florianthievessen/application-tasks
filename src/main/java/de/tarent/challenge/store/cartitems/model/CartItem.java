package de.tarent.challenge.store.cartitems.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.products.model.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.AUTO;

/**
 * A CartItem represents a row in the `cart_item`-table.
 * <p>
 * Always has a {@link CartItem#quantity} larger than 0 of a specific {@link Product}, and belongs to a {@link Cart}.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see Cart
 * @see de.tarent.challenge.store.carts.api.CartService
 * @see de.tarent.challenge.store.products.api.ProductService
 */
@Entity
public class CartItem {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @JsonIgnoreProperties
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id")
    private Cart cart;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column
    @Min(1)
    private Integer quantity;

    public CartItem() {
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Calculates the sum of this CartItem based on the products price and the current quantity.
     *
     * @return the sum of this CartItem
     */
    @JsonIgnore
    public BigDecimal getSum() {
        return product.getPrice().multiply(BigDecimal.valueOf(quantity));
    }

}
