package de.tarent.challenge.store.products.service;

import java.util.List;

import de.tarent.challenge.store.products.api.ProductCatalog;
import de.tarent.challenge.store.products.api.ProductService;
import de.tarent.challenge.store.products.model.Product;
import org.springframework.stereotype.Service;

/**
 * Default {@link Service}-implementation for handling {@link Product} business logic.
 *
 * @author unknown
 * @see ProductService
 * @see ProductCatalog
 */
@Service
public class DefaultProductService implements ProductService {

    private final ProductCatalog productCatalog;

    /**
     * Initializes this service with the given {@link ProductCatalog}
     *
     * @param productCatalog the {@link ProductCatalog}, that should be used by this service
     */
    public DefaultProductService(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    @Override
    public List<Product> retrieveAllProducts() {
        return productCatalog.findAll();
    }

    @Override
    public Product retrieveProductBySku(String sku) {
        return productCatalog.findBySku(sku);
    }

    @Override
    public Product save(Product product) {
        return productCatalog.save(product);
    }
}
