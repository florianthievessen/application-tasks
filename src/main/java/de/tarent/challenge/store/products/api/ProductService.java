package de.tarent.challenge.store.products.api;

import de.tarent.challenge.store.products.model.Product;
import de.tarent.challenge.store.products.service.DefaultProductService;

import java.util.List;

/**
 * Provides methods for accessing {@link Product}s.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see DefaultProductService
 */
public interface ProductService {

    /**
     * Returns a list of the currently existing {@link Product}s
     *
     * @return a list of {@link Product}s
     */
    List<Product> retrieveAllProducts();

    /**
     * Returns the {@link Product} with the given SKU
     *
     * @param sku the SKU, to find the {@link Product} for
     * @return the {@link Product}
     */
    Product retrieveProductBySku(String sku);

    /**
     * Persists a {@link Product} in the database
     *
     * @param product the {@link Product} to persist
     * @return the persisted {@link Product}
     */
    Product save(Product product);

}
