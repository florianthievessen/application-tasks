package de.tarent.challenge.store.products.api;

import de.tarent.challenge.store.products.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link JpaRepository}-implementation for {@link Product}s.
 * <p>
 * Provides access to {@link Product}s in the `product`-table.
 *
 * @author unknown
 */
public interface ProductCatalog extends JpaRepository<Product, Long> {

    /**
     * Finds a specific {@link Product} by its SKU.
     *
     * @param sku the {@link Product}s SKU
     * @return the {@link Product}
     */
    Product findBySku(String sku);

}
