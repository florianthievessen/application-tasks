package de.tarent.challenge.store.products.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;
import de.tarent.challenge.store.cartitems.model.CartItem;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import static javax.persistence.GenerationType.AUTO;

/**
 * A Product represents a row in the `product`-table.
 *
 * @author unkown
 */
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    /**
     * An SKU (<a href="https://en.wikipedia.org/wiki/Stock_keeping_unit">Stock Keeping Unit</a>) is a distinct type of
     * item for sale, purchased, or tracked in inventory, such as a product or service, and all attributes associated
     * with the item type that distinguish it from other item types.
     */
    @Column(unique = true, nullable = false)
    @NotBlank(message = "SKU can't be blank")
    private String sku;

    @Column(nullable = false)
    @NotBlank(message = "Name can't be blank")
    private String name;

    @Column(nullable = false)
    @NotNull(message = "Price can't be blank")
    @DecimalMin(value = "0.0", inclusive = false, message = "Price should be larger than 0.0")
    @Digits(integer = 13, fraction = 2)
    private BigDecimal price;

    @Column(nullable = false)
    private String currency;

    /**
     * EAN (<a href="https://en.wikipedia.org/wiki/International_Article_Number">European/International Article Number</a>)
     * is a standard describing a barcode symbology and numbering system used in global trade to identify a specific retail product
     * type, in a specific packaging configuration, from a specific manufacturer.
     * <p>
     * The eans-{@link Set} referres to the `product_eans`-table.
     */
    @ElementCollection
    @Fetch(FetchMode.JOIN)
    @NotEmpty(message = "Product should have at least one EAN")
    private Set<String> eans;

    @JsonIgnoreProperties
    @OneToMany(mappedBy = "product")
    private List<CartItem> cartItems = new ArrayList<>();

    public Product() {
    }

    /**
     * Initializes a new Product with an SKU, name, price, currency and eans
     *
     * @param sku      the Products SKU
     * @param name     the Products name
     * @param price    the Products price
     * @param currency the Products currency
     * @param eans     the Products EANs
     */
    public Product(String sku, String name, BigDecimal price, String currency, Set<String> eans) {
        this.sku = sku;
        this.name = name;
        this.price = price;
        this.currency = currency;
        this.eans = Collections.unmodifiableSet(eans);
    }

    public String getSku() {
        return sku;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public Set<String> getEans() {
        return Sets.newHashSet(eans);
    }

    /**
     * Updates this Product with the values from the given Product.
     *
     * @param product the Product, containing the new attribute values
     */
    public void update(Product product) {
        this.sku = product.getSku();
        this.name = product.getName();
        this.price = product.getPrice();
        this.currency = product.getCurrency();
        this.eans = Sets.newHashSet(product.getEans());
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {return true;}
        if (other == null || getClass() != other.getClass()) {return false;}
        Product product = (Product) other;
        return Objects.equals(id, product.id) && Objects.equals(sku, product.sku) && Objects.equals(name, product.name) && Objects.equals(price,
                                                                                                                                          product.price) && Objects.equals(
                currency, product.currency) && Objects.equals(eans, product.eans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sku, name, price, currency, eans);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id).add("sku", sku).add("name", name).add("price", price).add("currency",
                                                                                                                        currency).add("eans",
                                                                                                                                      eans).toString();
    }
}
