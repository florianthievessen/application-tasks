package de.tarent.challenge.store.products.rest;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import com.google.common.collect.Sets;
import de.tarent.challenge.store.products.api.ProductService;
import de.tarent.challenge.store.products.model.Product;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * {@link RestController} handling {@link Product} requests.
 *
 * @author unknown
 */
@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    /**
     * Initializes this controller with the given {@link ProductService}
     *
     * @param productService the {@link ProductService}, that this controller should use
     */
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Returns all available {@link Product}s.
     *
     * @return {@link Iterable} of {@link Product}s
     */
    @GetMapping
    public Iterable<Product> retrieveProducts() {
        return productService.retrieveAllProducts();
    }

    /**
     * Returns the {@link Product} corresponding to the given SKU
     *
     * @param sku the SKU, to find the {@link Product} for
     *
     * @return the {@link Product} with the given sku
     */
    @GetMapping("/{sku}")
    public Product retrieveProductBySku(@PathVariable String sku) {
        return productService.retrieveProductBySku(sku);
    }

    /**
     * Creates and persists a new {@link Product}.
     *
     * @param product the {@link Product}, to create and persist
     *
     * @return the new {@link Product}
     */
    @PostMapping
    public Product createProduct(@Valid @RequestBody Product product) {
        // TODO: refactor
        product.getEans().forEach(ean -> {
            if (ean.isEmpty()) {
                throw new ConstraintViolationException("An EAN can't be blank", Sets.newHashSet());
            }
        });
        return productService.save(product);
    }

    /**
     * Updates the {@link Product}.
     *
     * @param sku     the SKU of the {@link Product}, to update
     * @param product the updated {@link Product}
     *
     * @return the updated {@link Product}
     */
    @PutMapping("/{sku}")
    public Product updateProduct(
            @PathVariable String sku, @Valid @RequestBody Product product) {
        // TODO: refactor
        product.getEans().forEach(ean -> {
            if (ean.isEmpty()) {
                throw new ConstraintViolationException("An EAN can't be blank", Sets.newHashSet());
            }
        });
        Product existingProduct = productService.retrieveProductBySku(sku);
        existingProduct.update(product);

        return productService.save(existingProduct);
    }
}
