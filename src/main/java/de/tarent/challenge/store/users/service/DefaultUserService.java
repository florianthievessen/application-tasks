package de.tarent.challenge.store.users.service;

import de.tarent.challenge.store.products.api.ProductCatalog;
import de.tarent.challenge.store.users.api.UserRepository;
import de.tarent.challenge.store.users.api.UserService;
import de.tarent.challenge.store.users.model.User;
import org.springframework.stereotype.Service;

/**
 * Default {@link UserService}-implementation for handling {@link User} business logic.
 *
 * @author unknown
 * @see UserService
 * @see UserRepository
 * @see ProductCatalog
 */
@Service
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;

    /**
     * Initializes this service with the given {@link UserRepository}
     *
     * @param userRepository the {@link UserRepository}, that should be used by this service
     */
    public DefaultUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId);
    }
}
