package de.tarent.challenge.store.users.api;

import de.tarent.challenge.store.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link JpaRepository}-implementation for {@link User}s.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see User
 * @see UserService
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Finds a {@link User} by its userId.
     *
     * @param userId the Id, of the {@link User} to find
     *
     * @return the {@link User} with the given id, if found. Otherwise null
     */
    User findById(Long userId);
}