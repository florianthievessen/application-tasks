package de.tarent.challenge.store.users.api;

import de.tarent.challenge.store.users.model.User;

/**
 * Provides methods for accessing {@link User}s.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see de.tarent.challenge.store.users.service.DefaultUserService
 */
public interface UserService {

    /**
     * Returns the User with the given userId.
     *
     * @param userId the id, of the User to find
     * @return the User with the given userId
     */
    User getUserById(Long userId);

}
