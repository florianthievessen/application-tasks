package de.tarent.challenge.store.users.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;
import de.tarent.challenge.store.carts.model.Cart;
import org.hibernate.validator.constraints.NotBlank;

import static javax.persistence.GenerationType.AUTO;

/**
 * A User represents a row in the `user`-table.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @Column(nullable = false)
    @NotBlank(message = "Firstname can't be blank")
    private String firstName;

    @Column(nullable = false)
    @NotBlank(message = "Lastname can't be blank")
    private String lastName;

    @Column(nullable = false)
    @NotBlank(message = "Email-Address can't be blank")
    private String emailAddress;

    @JsonIgnoreProperties
    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER)
    private Cart cart;

    public User() {
    }

    /**
     * Initializes a new User with the given firstname, lastname and email-address
     *
     * @param firstName    the users firstname
     * @param lastName     the users lastname
     * @param emailAddress the users email-address
     */
    public User(String firstName, String lastName, String emailAddress) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Updates the values of this User with the given Users values.
     *
     * @param user the updated User
     */
    public void update(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.emailAddress = user.getEmailAddress();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {return true;}
        if (other == null || getClass() != other.getClass()) {return false;}
        User user = (User) other;
        return Objects.equals(id, user.id) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName,
                                                                                                          user.lastName) && Objects.equals(
                emailAddress, user.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, emailAddress);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id).add("firstName", firstName).add("lastName", lastName).add("emailAddress",
                                                                                                                        emailAddress).toString();
    }
}
