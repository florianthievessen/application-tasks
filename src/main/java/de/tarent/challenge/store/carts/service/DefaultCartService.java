package de.tarent.challenge.store.carts.service;

import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.carts.api.CartRepository;
import de.tarent.challenge.store.carts.api.CartService;
import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.products.api.ProductService;
import de.tarent.challenge.store.products.model.Product;
import de.tarent.challenge.store.users.api.UserService;
import org.springframework.stereotype.Service;

/**
 * Default {@link Service}-implementation for handling {@link Cart} business logic.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see CartRepository
 * @see ProductService
 * @see de.tarent.challenge.store.users.api.UserService
 */
@Service
public class DefaultCartService implements CartService {

    private final CartRepository cartRepository;

    private final ProductService productService;

    private final UserService userService;

    public DefaultCartService(CartRepository cartRepository, ProductService productService, UserService userService) {
        this.cartRepository = cartRepository;
        this.productService = productService;
        this.userService = userService;
    }

    @Override
    public Cart getCartByUserId(Long userId) {
        Cart cart = cartRepository.findFirstByUserId(userId);
        if (cart == null) {
            return Cart.EMPTY;
        }
        return cart;
    }

    @Override
    public Cart updateCart(Long userId, Cart updatedCart) {
        Cart existingCart = cartRepository.findFirstByUserId(userId);

        if (existingCart == null) {
            if (updatedCart.getCartItems().isEmpty()) {
                return Cart.EMPTY;
            }
            Cart cart = new Cart();
            cart.setUser(userService.getUserById(userId));
            updateCart(updatedCart, cart);
            return cartRepository.save(cart);
        }

        if (updatedCart.getCartItems().isEmpty()) {
            cartRepository.delete(existingCart);
            return Cart.EMPTY;
        } else {
            existingCart.getCartItems().clear();
            updateCart(updatedCart, existingCart);
            return cartRepository.save(existingCart);
        }
    }

    @Override
    public Cart checkoutCart(Long userId) {
        Cart cart = cartRepository.findFirstByUserId(userId);
        cart.setCheckedOut(true);
        return cartRepository.save(cart);
    }

    /**
     * Updates the existingCart with the updatedCarts {@link CartItem}s.
     *
     * @param updatedCart  the Cart containing an updated version of {@link CartItem}s
     * @param existingCart the Cart, whose {@link CartItem}s should get replaced
     */
    private void updateCart(Cart updatedCart, Cart existingCart) {
        for (CartItem newCartItem : updatedCart.getCartItems()) {
            Product product = productService.retrieveProductBySku(newCartItem.getProduct().getSku());
            newCartItem.setProduct(product);
            existingCart.addCartItem(newCartItem);
        }
    }

}
