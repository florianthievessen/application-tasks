package de.tarent.challenge.store.carts.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;
import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.users.model.User;

import static javax.persistence.GenerationType.AUTO;

/**
 * A Cart represents a row in the `cart`-table.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@Entity
public class Cart {

    public static final Cart EMPTY = new Cart();

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @JsonIgnoreProperties
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<CartItem> cartItems = new ArrayList<>();

    private boolean checkedOut;

    public Cart() {
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public BigDecimal getSum() {
        // TODO: respect currency!
        // TODO: refactor to money
        return cartItems.stream().map(CartItem::getSum).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Adds the {@link CartItem} to this Cart.
     *
     * @param newCartItem the {@link CartItem} to add to this Cart
     */
    public void addCartItem(CartItem newCartItem) {
        newCartItem.setCart(this);
        cartItems.add(newCartItem);
    }

    public boolean getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {return true;}
        if (other == null || getClass() != other.getClass()) {return false;}
        Cart cart = (Cart) other;
        return Objects.equals(id, cart.id) && Objects.equals(user, cart.user) && Objects.equals(cartItems, cart.getCartItems());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, cartItems);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id).add("sku", user).add("cartItems", cartItems).toString();
    }
}
