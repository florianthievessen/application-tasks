package de.tarent.challenge.store.carts.rest;

import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.carts.api.CartService;
import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.users.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * {@link RestController} handling {@link Cart} requests.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RestController
public class CartController {

    private final CartService cartService;

    /**
     * Initializes this service with the given {@link CartService}
     *
     * @param cartService {@link CartService} that should be used for this service
     */
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    /**
     * Returns the {@link User}s current {@link Cart}.
     *
     * @param userId the Users userId
     *
     * @return the Cart
     */
    @GetMapping("/users/{userId}/cart")
    public Cart getCart(@PathVariable Long userId) {
        return cartService.getCartByUserId(userId);
    }

    /**
     * Updates the {@link User}s current {@link Cart}, with the {@link CartItem}s of the given {@link Cart}.
     *
     * @param userId the Users userId
     * @param cart   the new {@link Cart}
     *
     * @return the updated Cart
     */
    @PutMapping("/users/{userId}/cart/items")
    public Cart updateCart(@PathVariable Long userId, @RequestBody Cart cart) {
        Cart existingCart = cartService.getCartByUserId(userId);
        if (existingCart.getCheckedOut()) {
            return existingCart;
        }
        return cartService.updateCart(userId, cart);
    }

    /**
     * Checks out the Users cart.
     *
     * @param userId the Users userId
     *
     * @return the checked out Cart
     */
    @PutMapping("/users/{userId}/cart/checkout")
    public Cart checkoutCart(@PathVariable Long userId) {
        return cartService.checkoutCart(userId);
    }
}
