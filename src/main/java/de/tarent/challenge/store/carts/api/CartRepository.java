package de.tarent.challenge.store.carts.api;

import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link JpaRepository} for {@link Cart}s.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see Cart
 * @see CartService
 * @see de.tarent.challenge.store.carts.rest.CartController
 * @see de.tarent.challenge.store.cartitems.model.CartItem
 */
public interface CartRepository extends JpaRepository<Cart, Long> {

    /**
     * Finds the Cart, that belongs to the {@link User} with the given userId.
     *
     * @param userId the id, of the {@link User}, to find the Cart for
     * @return the {@link User}s cart.
     */
    Cart findFirstByUserId(Long userId);

}