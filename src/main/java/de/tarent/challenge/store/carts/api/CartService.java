package de.tarent.challenge.store.carts.api;

import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link JpaRepository} for Carts.
 *
 * @author Florian Thievessen, 15.01.2022
 * @see Cart
 * @see CartService
 * @see de.tarent.challenge.store.cartitems.model.CartItem
 */
public interface CartService {

    /**
     * Finds the Cart, that belongs to the {@link User} with the given userId.
     *
     * @param userId the id, of the {@link User}, to find the Cart for
     *
     * @return the {@link User}s cart.
     */
    Cart getCartByUserId(Long userId);

    /**
     * Updates the Cart, that belongs to the given {@link User} with the {@link CartItem}s of the new Cart.
     *
     * @param userId the id, of the {@link User}, to find the Cart for
     * @param cart   the new {@link Cart}
     *
     * @return the {@link User}s cart.
     */
    Cart updateCart(Long userId, Cart cart);

    /**
     * Checks out the Cart, that belongs to the given {@link User}.
     *
     * @param userId the id, of the {@link User}, to check the Cart out for.
     *
     * @return the {@link User}s checked out cart.
     */
    Cart checkoutCart(Long userId);
}
