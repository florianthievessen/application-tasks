INSERT INTO product (id, name, sku, price, currency) VALUES (1, 'Milch', '102', 1.19, 'EUR');
INSERT INTO product (id, name, sku, price, currency) VALUES (2, 'Brot', '2035', 2.49, 'EUR');
INSERT INTO product (id, name, sku, price, currency) VALUES (3, 'Käse', 'S-155', 2.69, 'EUR');
INSERT INTO product (id, name, sku, price, currency) VALUES (4, 'Wurst', '1488', 3.19, 'EUR');
INSERT INTO product (id, name, sku, price, currency) VALUES (5, 'Couscous', 'B001', 3.69, 'EUR');

INSERT INTO product_eans (product_id, eans) VALUES (1, '12345678');
INSERT INTO product_eans (product_id, eans) VALUES (1, '77777777');
INSERT INTO product_eans (product_id, eans) VALUES (1, '23498128');
INSERT INTO product_eans (product_id, eans) VALUES (2, '34558821');
INSERT INTO product_eans (product_id, eans) VALUES (2, '12323410');
INSERT INTO product_eans (product_id, eans) VALUES (3, '34598146');
INSERT INTO product_eans (product_id, eans) VALUES (3, '43565922');
INSERT INTO product_eans (product_id, eans) VALUES (3, '23454045');
INSERT INTO product_eans (product_id, eans) VALUES (4, '18754629');
INSERT INTO product_eans (product_id, eans) VALUES (4, '46025548');
INSERT INTO product_eans (product_id, eans) VALUES (5, '54342316');

INSERT INTO user (id, first_name, last_name, email_address) VALUES (1, 'Max', 'Mustermann', 'max.mustermann@tarent.de');
INSERT INTO user (id, first_name, last_name, email_address) VALUES (2, 'Marta', 'Musterfrau', 'marta.musterfrau@tarent.de');

INSERT INTO cart (id, user_id, checked_out) VALUES (1, 1, FALSE);

INSERT INTO cart_item (id, cart_id, product_id, quantity) VALUES (1, 1, 1, 5);
INSERT INTO cart_item (id, cart_id, product_id, quantity) VALUES (2, 1, 2, 1);
INSERT INTO cart_item (id, cart_id, product_id, quantity) VALUES (3, 1, 3, 2);