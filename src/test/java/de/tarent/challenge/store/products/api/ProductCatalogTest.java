package de.tarent.challenge.store.products.api;

import com.google.common.collect.ImmutableSet;
import de.tarent.challenge.store.products.model.Product;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;

/**
 * {@link DataJpaTest} for the {@link ProductCatalog}.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductCatalogTest {

    @Autowired
    ProductCatalog productCatalog;

    @Test
    public void testShouldFindProductBySku() {
        Product product = productCatalog.findBySku("2035");
        Assertions.assertThat(product).isNotNull();
        Assertions.assertThat(product.getSku()).isEqualTo("2035");
        Assertions.assertThat(product.getName()).isEqualTo("Brot");
        Assertions.assertThat(product.getEans()).isEqualTo(ImmutableSet.of("12323410", "34558821"));
    }

    @Test
    public void testShouldPersistProduct() {
        Product butter = new Product("2036", "Butter", BigDecimal.valueOf(2.99), "EUR", ImmutableSet.of("11111111", "22222222", "33333333"));
        Product persistedButter = productCatalog.save(butter);

        Assertions.assertThat(persistedButter).isNotNull();
        Assertions.assertThat(persistedButter.getSku()).isEqualTo("2036");
        Assertions.assertThat(persistedButter.getName()).isEqualTo("Butter");
        Assertions.assertThat(persistedButter.getEans()).isEqualTo(ImmutableSet.of("11111111", "22222222", "33333333"));
    }

    @Test
    public void testUpdatePersistedProduct() {
        Product existingBread = productCatalog.findBySku("2035");
        Product bread = new Product("2035-2", "Frisches Brot", BigDecimal.valueOf(2.99), "EUR", new HashSet<>(Arrays.asList("44444444", "55555555", "66666666")));
        existingBread.update(bread);

        productCatalog.save(existingBread);

        Product updatedBread = productCatalog.findBySku("2035-2");
        Assertions.assertThat(updatedBread).isNotNull();
        Assertions.assertThat(updatedBread.getSku()).isEqualTo("2035-2");
        Assertions.assertThat(updatedBread.getName()).isEqualTo("Frisches Brot");
        Assertions.assertThat(updatedBread.getEans()).isEqualTo(ImmutableSet.of("44444444", "55555555", "66666666"));
    }

}
