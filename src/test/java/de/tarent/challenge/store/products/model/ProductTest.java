package de.tarent.challenge.store.products.model;

import com.google.common.collect.ImmutableSet;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Unit-tests for the {@link Product}-class.
 *
 * @author Florian Thievessen, 15.01.2022
 */
public class ProductTest {

    @Test
    public void testProductsShouldEqual() {
        Product milk = new Product("102", "Milch", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("12345678", "77777777", "23498128"));
        Product otherMilk = new Product("102", "Milch", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("12345678", "77777777", "23498128"));

        Assertions.assertThat(milk).isEqualTo(otherMilk);
    }

    @Test
    public void testDifferentProductsShouldNotEqual() {
        Product milk = new Product("102", "Milch", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("12345678", "77777777", "23498128"));
        Product bread = new Product("2035", "Brot", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("34558821", "12323410"));

        Assertions.assertThat(milk).isNotEqualTo(bread);
    }

}
