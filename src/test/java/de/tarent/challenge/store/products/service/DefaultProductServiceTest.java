package de.tarent.challenge.store.products.service;

import com.google.common.collect.ImmutableSet;
import de.tarent.challenge.store.products.api.ProductService;
import de.tarent.challenge.store.products.model.Product;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

/**
 * Tests for the {@link ProductService}-implementation.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DefaultProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void testShouldRetrieveProducts() {
        List<Product> products = productService.retrieveAllProducts();

        Assertions.assertThat(products.size()).isEqualTo(5);

        Product product = products.get(0);
        Assertions.assertThat(product.getSku()).isEqualTo("102");
        Assertions.assertThat(product.getName()).isEqualTo("Milch");
        Assertions.assertThat(product.getEans()).isEqualTo(ImmutableSet.of("77777777", "23498128", "12345678"));
    }

    @Test
    public void testShouldRetrieveProductBySku() {
        Product product = productService.retrieveProductBySku("102");

        Assertions.assertThat(product.getSku()).isEqualTo("102");
        Assertions.assertThat(product.getName()).isEqualTo("Milch");
        Assertions.assertThat(product.getEans()).isEqualTo(ImmutableSet.of("77777777", "23498128", "12345678"));
    }

    @Test
    public void testShouldSaveProduct() {
        int initialProductCount = productService.retrieveAllProducts().size();

        Product productToSave = new Product("103", "Butter", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("11111111", "22222222", "33333333"));
        Product persistedProduct = productService.save(productToSave);

        Assertions.assertThat(persistedProduct.getSku()).isEqualTo("103");
        Assertions.assertThat(persistedProduct.getName()).isEqualTo("Butter");
        Assertions.assertThat(persistedProduct.getEans()).isEqualTo(ImmutableSet.of("11111111", "22222222", "33333333"));

        Assertions.assertThat(productService.retrieveAllProducts().size()).isEqualTo(initialProductCount + 1);
    }

    @Test
    public void testShouldUpdateProduct() {
        int initialProductCount = productService.retrieveAllProducts().size();

        Product product = new Product("103", "Milch jetzt teurer", BigDecimal.valueOf(1.29), "EUR", ImmutableSet.of("11111111", "22222222", "33333333"));
        Product existingProduct = productService.retrieveProductBySku("102");
        existingProduct.update(product);
        existingProduct = productService.save(existingProduct);

        Assertions.assertThat(existingProduct.getSku()).isEqualTo("103");
        Assertions.assertThat(existingProduct.getName()).isEqualTo("Milch jetzt teurer");
        Assertions.assertThat(existingProduct.getPrice()).isEqualTo(BigDecimal.valueOf(1.29));
        Assertions.assertThat(existingProduct.getCurrency()).isEqualTo("EUR");
        Assertions.assertThat(existingProduct.getEans()).isEqualTo(ImmutableSet.of("11111111", "22222222", "33333333"));

        Assertions.assertThat(productService.retrieveAllProducts().size()).isEqualTo(initialProductCount);
    }

    @Test
    @Ignore
    public void testShouldNotSaveProductWithoutOrEmptySku() {
        // TODO: implement
    }

    @Test
    @Ignore
    public void testShouldNotSaveProductWithoutOrEmptyName() {
        // TODO: implement
    }

    @Test
    @Ignore
    public void testShouldNotSaveProductWithoutOrEmptyEan() {
        // TODO: implement
    }

}
