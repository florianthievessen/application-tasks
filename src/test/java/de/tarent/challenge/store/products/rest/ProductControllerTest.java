package de.tarent.challenge.store.products.rest;

import com.google.common.collect.ImmutableSet;
import de.tarent.challenge.store.products.model.Product;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * Tests for the {@link ProductController}.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testShouldRetrieveProducts() {
        Product[] existingProducts = this.restTemplate.getForEntity(
                "http://localhost:" + port + "/products",
                Product[].class).getBody();

        Assertions.assertThat(existingProducts).isNotEmpty();
        Assertions.assertThat(existingProducts).isEqualTo(
                new Product[]{
                        new Product("102", "Milch", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("77777777", "23498128", "12345678")),
                        new Product("2035", "Brot", BigDecimal.valueOf(2.49), "EUR", ImmutableSet.of("12323410", "34558821")),
                        new Product("S-155", "Käse", BigDecimal.valueOf(2.69), "EUR", ImmutableSet.of("23454045", "43565922", "34598146")),
                        new Product("1488", "Wurst", BigDecimal.valueOf(3.19), "EUR", ImmutableSet.of("46025548", "18754629")),
                        new Product("B001", "Couscous", BigDecimal.valueOf(3.69), "EUR", ImmutableSet.of("54342316"))
                }
        );
    }

    @Test
    public void testShouldRetrieveProductBySku() {
        Product product = this.restTemplate.getForEntity(
                "http://localhost:" + port + "/products/102",
                Product.class).getBody();

        Assertions.assertThat(product).isEqualTo(new Product("102", "Milch", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("77777777", "23498128", "12345678")));
    }

    @Test
    public void testShouldCreateProduct() {
        Product product = new Product("103", "Neues Produkt", BigDecimal.valueOf(1.19), "EUR", ImmutableSet.of("11111111", "22222222", "33333333"));
        Product createdProduct = this.restTemplate.postForObject(
                "http://localhost:" + port + "/products",
                product,
                Product.class);

        Assertions.assertThat(createdProduct).isEqualTo(product);
    }

    @Test
    public void testShouldUpdateProduct() {
        Product product = new Product("102", "Milch Updated", BigDecimal.valueOf(1.29), "EUR", ImmutableSet.of("11111111", "22222222", "33333333"));
        this.restTemplate.put(
                "http://localhost:" + port + "/products/" + product.getSku(),
                product);

        Product updatedProduct = this.restTemplate.getForEntity(
                "http://localhost:" + port + "/products/" + product.getSku(),
                Product.class).getBody();

        Assertions.assertThat(updatedProduct).isEqualTo(
                new Product("102", "Milch Updated", BigDecimal.valueOf(1.29), "EUR", ImmutableSet.of("11111111", "22222222", "33333333"))
        );
    }

}
