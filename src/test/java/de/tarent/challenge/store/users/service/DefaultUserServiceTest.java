package de.tarent.challenge.store.users.service;

import de.tarent.challenge.store.users.api.UserService;
import de.tarent.challenge.store.users.model.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Tests for the {@link UserService}-implementation.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DefaultUserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void testShouldGetUserById() {
        User user = userService.getUserById(1L);
        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.getId()).isEqualTo(1L);
        Assertions.assertThat(user.getFirstName()).isEqualTo("Max");
        Assertions.assertThat(user.getLastName()).isEqualTo("Mustermann");
        Assertions.assertThat(user.getEmailAddress()).isEqualTo("max.mustermann@tarent.de");
    }

}
