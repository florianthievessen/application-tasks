package de.tarent.challenge.store.users.api;

import de.tarent.challenge.store.users.model.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * {@link DataJpaTest} for the {@link UserRepository}.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void testShouldFindUserById() {
        User user = userRepository.findById(1L);
        Assertions.assertThat(user).isNotNull();
        Assertions.assertThat(user.getId()).isEqualTo(1L);
        Assertions.assertThat(user.getFirstName()).isEqualTo("Max");
        Assertions.assertThat(user.getLastName()).isEqualTo("Mustermann");
        Assertions.assertThat(user.getEmailAddress()).isEqualTo("max.mustermann@tarent.de");
    }

}
