package de.tarent.challenge.store.users.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Unit-tests for the {@link User}-class.
 *
 * @author Florian Thievessen, 15.01.2022
 */
public class UserTest {

    @Test
    public void testUserShouldHaveAFirstnameLastnameAndEmailAddress() {
        User user = new User("Firstname", "Lastname", "user@challenge-store.de");
        Assertions.assertThat(user.getFirstName()).isEqualTo("Firstname");
        Assertions.assertThat(user.getLastName()).isEqualTo("Lastname");
        Assertions.assertThat(user.getEmailAddress()).isEqualTo("user@challenge-store.de");
    }

    @Test
    public void testShouldUpdateFromUser() {
        User user = new User("Firstname", "Lastname", "user@challenge-store.de");
        User otherUser = new User("OtherFirstname", "OtherLastname", "otheruser@challenge-store.de");
        user.update(otherUser);
        Assertions.assertThat(user).isEqualTo(otherUser);
    }

}
