package de.tarent.challenge.store.carts.rest;

import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.carts.api.CartService;
import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.products.api.ProductService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * Tests for the {@link CartController}.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CartControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartService cartService;

    @Test
    public void testShouldGetCart() {
        Cart cart = this.restTemplate.getForEntity(
                "http://localhost:" + port + "/users/1/cart",
                Cart.class).getBody();

        Assertions.assertThat(cart).isNotNull();
        Assertions.assertThat(cart.getCartItems()).isNotEmpty();
        Assertions.assertThat(cart.getCartItems().size()).isEqualTo(3);
        Assertions.assertThat(cart.getSum()).isEqualTo(BigDecimal.valueOf(13.82));
    }

    @Test
    public void testShouldUpdateCartWithCartItems() {
        Cart cart = new Cart();
        CartItem cartItem1 = new CartItem();
        cartItem1.setProduct(productService.retrieveProductBySku("102"));
        cartItem1.setQuantity(1);
        cart.addCartItem(cartItem1);

        this.restTemplate.put(
                "http://localhost:" + port + "/users/1/cart/items",
                cart,
                Cart.class);

        Cart updatedCart = cartService.getCartByUserId(1L);

        Assertions.assertThat(updatedCart).isNotNull();
        Assertions.assertThat(updatedCart.getCartItems()).isNotEmpty();
        Assertions.assertThat(updatedCart.getCartItems().size()).isEqualTo(1);
        Assertions.assertThat(updatedCart.getSum()).isEqualTo(BigDecimal.valueOf(1.19));
    }

    @Test
    public void testShouldUpdateToEmptyCart() {
        this.restTemplate.put(
                "http://localhost:" + port + "/users/1/cart/items",
                new Cart(),
                Cart.class);

        Cart cart = cartService.getCartByUserId(1L);

        Assertions.assertThat(cart).isNotNull();
        Assertions.assertThat(cart.getCartItems()).isEmpty();
        Assertions.assertThat(cart.getSum()).isEqualTo(BigDecimal.valueOf(0));
    }

    @Test
    public void testShouldCheckoutCart() {
        Cart cart = cartService.getCartByUserId(1L);
        Assertions.assertThat(cart.getCheckedOut()).isFalse();

        this.restTemplate.put(
                "http://localhost:" + port + "/users/1/cart/checkout",
                cart);

        cart = cartService.getCartByUserId(1L);

        Assertions.assertThat(cart).isNotNull();
        Assertions.assertThat(cart.getCartItems()).isNotEmpty();
        Assertions.assertThat(cart.getCartItems().size()).isEqualTo(3);
        Assertions.assertThat(cart.getSum()).isEqualTo(BigDecimal.valueOf(13.82));
        Assertions.assertThat(cart.getCheckedOut()).isTrue();
    }

    @Test
    public void testCheckoutCartShouldNotGetChangedAnymore() {
        Cart cart = cartService.getCartByUserId(1L);
        Assertions.assertThat(cart.getCartItems().size()).isEqualTo(3);

        this.restTemplate.put(
                "http://localhost:" + port + "/users/1/cart/checkout",
                cart);

        Cart cartUpdate = cartService.getCartByUserId(1L);
        Assertions.assertThat(cartUpdate.getCheckedOut()).isTrue();

        CartItem cartItem1 = new CartItem();
        cartItem1.setProduct(productService.retrieveProductBySku("102"));
        cartItem1.setQuantity(1);
        cartUpdate.addCartItem(cartItem1);

        this.restTemplate.put(
                "http://localhost:" + port + "/users/1/cart/items",
                cartUpdate,
                Cart.class);

        Cart updatedCart = cartService.getCartByUserId(1L);

        Assertions.assertThat(updatedCart.getCartItems().size()).isEqualTo(3);
    }

}
