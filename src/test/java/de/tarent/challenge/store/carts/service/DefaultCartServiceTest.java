package de.tarent.challenge.store.carts.service;

import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.carts.api.CartService;
import de.tarent.challenge.store.carts.model.Cart;
import de.tarent.challenge.store.products.api.ProductService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * Tests for the {@link DefaultCartService}-implementation.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DefaultCartServiceTest {

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    @Test
    public void testShouldGetCartByUserId() {
        Cart cart = cartService.getCartByUserId(1L);

        Assertions.assertThat(cart).isNotNull();
        Assertions.assertThat(cart.getCartItems()).isNotNull();
        Assertions.assertThat(cart.getCartItems().size()).isEqualTo(3);
        Assertions.assertThat(cart.getSum()).isEqualTo(BigDecimal.valueOf(13.82));
    }

    @Test
    public void testShouldUpdateCart() {
        Cart cart = new Cart();
        CartItem cartItem1 = new CartItem();
        cartItem1.setProduct(productService.retrieveProductBySku("102"));
        cartItem1.setQuantity(1);
        cart.addCartItem(cartItem1);

        cartService.updateCart(1L, cart);
    }

    @Test
    public void testShouldCheckoutCart() {
        Cart cart = new Cart();
        CartItem cartItem1 = new CartItem();
        cartItem1.setProduct(productService.retrieveProductBySku("102"));
        cartItem1.setQuantity(1);
        cart.addCartItem(cartItem1);

        Assertions.assertThat(cart.getCheckedOut()).isFalse();

        cart = cartService.checkoutCart(1L);

        Assertions.assertThat(cart.getCheckedOut()).isTrue();
    }

}
