package de.tarent.challenge.store.carts.api;

import de.tarent.challenge.store.carts.model.Cart;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * {@link DataJpaTest} for the {@link CartRepository}.
 *
 * @author Florian Thievessen, 15.01.2022
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CartRepositoryTest {

    @Autowired
    CartRepository cartRepository;

    @Test
    public void testShouldFindProductBySku() {
        Cart cart = cartRepository.findFirstByUserId(1L);
        Assertions.assertThat(cart).isNotNull();
        Assertions.assertThat(cart.getSum()).isEqualTo(BigDecimal.valueOf(13.82));
        Assertions.assertThat(cart.getCartItems().size()).isEqualTo(3);
    }

}
