package de.tarent.challenge.store.carts.model;

import de.tarent.challenge.store.cartitems.model.CartItem;
import de.tarent.challenge.store.users.model.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Unit-tests for the {@link Cart}-class.
 *
 * @author Florian Thievessen, 15.01.2022
 */
public class CartTest {

    @Test
    public void testCartHaveCartItems() {
        Cart cart = new Cart();
        CartItem newCartItem = new CartItem();
        cart.addCartItem(newCartItem);
        Assertions.assertThat(cart.getCartItems()).isNotEmpty();
    }

    @Test
    public void testInitialCartShouldBeEmpty() {
        Cart cart = new Cart();
        Assertions.assertThat(cart.getCartItems()).isEmpty();
    }

    @Test
    public void testCartShouldBelongToUser() {
        Cart cart = new Cart();
        User user = new User();
        cart.setUser(user);
    }

    @Test
    public void testCartShouldGetCheckedOut() {
        Cart cart = new Cart();
        Assertions.assertThat(cart.getCheckedOut()).isFalse();
        cart.setCheckedOut(true);
        Assertions.assertThat(cart.getCheckedOut()).isTrue();
    }


}
