package de.tarent.challenge.store.cartitems.model;

import de.tarent.challenge.store.products.model.Product;
import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Unit-tests for the {@link CartItem}-class.
 *
 * @author Florian Thievessen, 15.01.2022
 */
public class CartItemTest {

    @Test
    public void testCartItemShouldHaveAProduct() {
        CartItem cartItem = new CartItem();
        Product product = new Product();
        cartItem.setProduct(product);

        Assertions.assertThat(cartItem.getProduct()).isEqualTo(product);
    }

}
